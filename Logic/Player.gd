extends Node2D

var display_name = ""
var units = []


func get_display_name():
    return display_name


func get_units():
    return units


func _ready():
    var errno = GlobalMatchState.connect("active_player_changed", self, "_on_active_player_changed")
    assert(errno == OK)


func _on_active_player_changed(last_player):
    if self == last_player:
        for unit in units:
            unit.init()


func init(new_display_name):
    display_name = new_display_name


func destroy():
    queue_free()


func add_unit(unit):
    units.append(unit)


func remove_unit(unit):
    units.erase(unit)
