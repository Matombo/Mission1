extends "res://Logic/Marker/MoveMarkerReachable.gd"

func _input_event(_viewport, event, _shape_idx):
    if event.is_action_pressed("mouse_button_left"):
        var unit = get_unit()
        # MoveMarkerImmediate must always be child of Unit
        assert(unit)
        
        unit.take_step(tile_location)
