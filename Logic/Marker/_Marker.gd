extends Area2D


# Collision ID's work as a bit mask. They are set in the *.tscn files.
const MOVEMARKERREACHABLE_COLLISION_ID = 4
const MOVEMARKERIMMEDIATE_COLLISION_ID = 8
const HEALTHMARKER_COLLISION_ID = 16
const ATTACKMARKER_COLLISION_ID = 32
const UNIT_COLLISION_ID  = 64
const _MARKER_COLLISION_ID = MOVEMARKERREACHABLE_COLLISION_ID | MOVEMARKERIMMEDIATE_COLLISION_ID | \
                             HEALTHMARKER_COLLISION_ID | ATTACKMARKER_COLLISION_ID | UNIT_COLLISION_ID


func get_marker_here(collision_id):
    var global_position = get_global_transform().get_origin() + GlobalConstants.tile_vector_half
    var direct_space_state = get_world_2d().get_direct_space_state()
    return direct_space_state.intersect_point(global_position, 32, [], collision_id, false, true)


func get_marker_at_tile_offset(collision_id, tile_offset):
    var global_position = get_global_transform().get_origin() + GlobalConstants.tile_vector_half \
                          + tile_offset * GlobalConstants.tile_size
    var direct_space_state = get_world_2d().get_direct_space_state()
    return direct_space_state.intersect_point(global_position, 32, [], collision_id, false, true)


func get_moveable_at_tile_offset(tile_location, tile_offset):
    var map = GlobalMatchState.get_loaded_map()
    assert(map)
    var offset_tile_location = tile_location + tile_offset
    var tile = map.get_cellv(offset_tile_location)
    if tile == map.INVALID_CELL:
        return false
    
    var marker_at_offset = get_marker_at_tile_offset(HEALTHMARKER_COLLISION_ID | UNIT_COLLISION_ID, tile_offset)
    assert(marker_at_offset.size() <= 1)
    if marker_at_offset.size():
        assert(marker_at_offset[0].collider.has_method("get_unit"))
        if marker_at_offset[0].collider.get_unit() != self.get_unit():
            return false
    
    return true


func get_unit():
    var parent = get_parent().owner
    if parent:
        assert(parent.has_method("get_unit"))
        return parent.get_unit()
