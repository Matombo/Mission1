extends "res://Logic/Marker/_Marker.gd"

var move_marker_reachable = load("res://Logic/Marker/MoveMarkerReachable.tscn")

var movement_points_left = 0
var tile_location = Vector2(0, 0)


func get_movement_points_left():
    return movement_points_left


func init(new_movement_points_left, new_tile_location):
    movement_points_left = new_movement_points_left
    tile_location = new_tile_location
    
    if movement_points_left:
        for i in range(4):
            if get_marker_at_tile_offset(_MARKER_COLLISION_ID, GlobalConstants.sign_offsets[i]).size() == 0:
                var map = GlobalMatchState.get_loaded_map()
                assert(map)
                var neightbor_tile_location = tile_location + GlobalConstants.sign_offsets[i]
                var tile = map.get_cellv(neightbor_tile_location)
                if tile != map.INVALID_CELL:
                    var tmp = move_marker_reachable.instance()
                    tmp.position = GlobalConstants.tile_neightbors_offsets[i]
                    add_child(tmp)
                    tmp.init(movement_points_left - 1, neightbor_tile_location)
