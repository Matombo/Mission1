extends "res://Logic/Marker/Units/Unit.gd"


func _ready():
    display_name = "Hack 1.0"
    health_points = 10
    movement_points = 10
    attack_damage = 2
    attack_range_minimum = 1
    attack_range_maximum = 1
    
    init() # FIXME don't call this automatically but by signal when match starts
