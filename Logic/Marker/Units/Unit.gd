extends "res://Logic/Marker/_Marker.gd"

# Imports
var move_marker_immediate_tscn = preload("res://Logic/Marker/MoveMarkerImmediate.tscn")
var attack_marker_tscn = preload("res://Logic/Marker/AttackMarker.tscn")
var health_marker_tscn = preload("res://Logic/Marker/HealthMarker.tscn")

# Unit Properties
var display_name = ""
var health_points = 0
var movement_points = 0
var attack_damage = 0
var attack_range_minimum = 0
var attack_range_maximum = 0

# Unit Status
var controlling_player = null
var unit_ready = false
var movement_points_current = 0
var attack_markers = null
var health_markers = null
var move_markers = null


func get_unit():
    return self


func get_attack_damage():
    return attack_damage


func set_controlling_player(new_controlling_player=null):
    if controlling_player:
        controlling_player.remove_unit(self)
    controlling_player = new_controlling_player


func get_display_name():
    return display_name


func get_unit_ready():
    return unit_ready


func set_unit_ready_false():
    unit_ready = false


func set_position_to_tile(tile_location):
    position = tile_location * GlobalConstants.tile_size


func _ready():
    # Make sure unit is not placed out of grid
    assert(position == position.snapped(GlobalConstants.tile_vector))
    
    attack_markers = get_node("AttackMarkers")
    health_markers = get_node("HealthMarkers")
    move_markers = get_node("MoveMarkers")


func init():
    movement_points_current = movement_points
    unit_ready = true


func _input_event(_viewport, event, _shape_idx):
    if controlling_player == GlobalMatchState.get_active_player() and event.is_action_pressed("mouse_button_left"):
        # unselect by clicking selected unit
        if self == GlobalMatchState.get_selected_unit():
            GlobalMatchState.select_unit()
        # select by clicking unselected unit
        else:
            GlobalMatchState.select_unit(self)


func select():
    if unit_ready:
        create_move_markers()


func deselect():
    # TODO if unit already moved or attacked set unit_ready to false
    destroy_markers()


func create_move_markers():
    destroy_markers()
    if unit_ready and movement_points_current > 0:
        for i in range(4):
            var tile_location = position / GlobalConstants.tile_size
            if get_moveable_at_tile_offset(tile_location, GlobalConstants.sign_offsets[i]):
                var neightbor_tile_location = tile_location + GlobalConstants.sign_offsets[i]
                var tmp = move_marker_immediate_tscn.instance()
                tmp.position = GlobalConstants.tile_neightbors_offsets[i]
                move_markers.add_child(tmp)
                tmp.init(movement_points_current - 1, neightbor_tile_location)


func create_attack_markers():
    destroy_markers()
    if unit_ready:
        for i in range(4):
            var tmp = attack_marker_tscn.instance()
            tmp.position = GlobalConstants.tile_neightbors_offsets[i]
            attack_markers.add_child(tmp)


func destroy_markers():
    for child in move_markers.get_children():
        child.queue_free()
    for child in attack_markers.get_children():
        child.queue_free()


func take_step(tile_location):
    var offset = position - tile_location * GlobalConstants.tile_size
    
    set_position_to_tile(tile_location)
    movement_points_current -= 1
    create_move_markers()
    
    for marker in health_markers.get_children():
        marker.position += offset
    
    var tmp = health_marker_tscn.instance()
    tmp.position = offset
    health_markers.add_child(tmp)
    
    var overlapping_health_markers = get_marker_here(HEALTHMARKER_COLLISION_ID)
    assert(overlapping_health_markers.size() <= 2)
    if overlapping_health_markers.size() == 2:
        overlapping_health_markers[1].collider.queue_free()
        # Remove child manually because health markers must be updated for next if-statement
        health_markers.remove_child(overlapping_health_markers[1].collider)
    if health_markers.get_child_count() > health_points:
        health_markers.get_child(0).queue_free()


func take_damage(damage):
    if health_markers.get_child_count() >= damage:
        for i in range(damage):
            health_markers.get_child(i).queue_free()
    else:
        if controlling_player:
            controlling_player.remove_unit(self)
        queue_free()
