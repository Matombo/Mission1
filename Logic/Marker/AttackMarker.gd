extends "res://Logic/Marker/_Marker.gd"


func _input_event(_viewport, event, _shape_idx):
    if event.is_action_pressed("mouse_button_left"):
        var unit = get_unit()
        # AttackMarker must always be child of Unit
        assert(unit)
        assert(unit.has_method("get_attack_damage"))
        assert(unit.has_method("set_unit_ready_false"))
        
        var overlapping_areas = get_overlapping_areas()
        # Tiles should only be occupied by singular Unit or HealthMarker
        assert(overlapping_areas.size() <= 1)
        if overlapping_areas.size():
            assert(overlapping_areas[0].has_method("get_unit"))
        
            var enemy = overlapping_areas[0].get_unit()
            if not enemy in GlobalMatchState.get_friendly_units():
                enemy.take_damage(unit.get_attack_damage())
        unit.set_unit_ready_false()
        GlobalMatchState.select_unit()
