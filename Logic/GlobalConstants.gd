extends Node

var tile_size = 64
var tile_vector = Vector2(tile_size, tile_size)
var tile_vector_half = Vector2(tile_size/2, tile_size/2)
var tile_neightbors_offsets = [Vector2(tile_size, 0),
                               Vector2(0, tile_size),
                               Vector2(-tile_size, 0),
                               Vector2(0, -tile_size)]
var sign_offsets = [Vector2(1, 0),
                    Vector2(0, 1),
                    Vector2(-1, 0),
                    Vector2(0, -1)]
