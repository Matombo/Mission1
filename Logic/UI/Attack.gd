extends Button


func _pressed():
    if GlobalMatchState.selected_unit:
        GlobalMatchState.selected_unit.create_attack_markers()


func _ready():
    var errno = GlobalMatchState.connect("selected_unit_changed", self, "_on_selected_unit_changed")
    assert(errno == OK)
    _on_selected_unit_changed()


func _on_selected_unit_changed():
    var unit = GlobalMatchState.get_selected_unit()
    if unit and unit.get_unit_ready():
        show()
    else:
        hide()
