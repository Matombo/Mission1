extends Button

var player_class = preload("res://Logic/Player.gd")
var hack_class = preload("res://Logic/Marker/Units/Hack10.tscn")


func _pressed():
    # FIXME mainly temporary setup code
    owner.owner.get_node("Menu").hide()
    owner.owner.get_node("HUD").show()
    owner.owner.get_node("MapContainer").show()
    
    var player_1 = player_class.new()
    player_1.init("Player 1")
    var player_2 = player_class.new()
    player_2.init("Player 2")
    var players = [player_1, player_2]
    for player in players:
        add_child(player)
    GlobalMatchState.init(owner.owner.get_node("HUD"), owner.owner.get_node("MapContainer"), "Map1", players)
    
    var tmp = hack_class.instance()
    tmp.set_position_to_tile(Vector2(2, 2))
    tmp.set_controlling_player(players[0])
    players[0].add_unit(tmp)
    GlobalMatchState.get_loaded_map().add_child(tmp)
    
    tmp = hack_class.instance()
    tmp.set_position_to_tile(Vector2(3, 2))
    tmp.set_controlling_player(players[1])
    players[1].add_unit(tmp)
    GlobalMatchState.get_loaded_map().add_child(tmp)
    
    var errno = GlobalMatchState.connect("match_destroyed", players[0], "destroy")
    assert(errno == OK)
    errno = GlobalMatchState.connect("match_destroyed", players[1], "destroy")
    assert(errno == OK)
    #EMXIF
