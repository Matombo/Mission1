extends Label


func _ready():
    var errno = GlobalMatchState.connect("active_player_changed", self, "_on_active_player_changed")
    assert(errno == OK)
    _on_active_player_changed(null)


func _on_active_player_changed(_last_player):
    var active_player = GlobalMatchState.get_active_player()
    if active_player:
        text = GlobalMatchState.get_active_player().get_display_name()
    else:
        text = ""
