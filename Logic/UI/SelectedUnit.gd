extends Label


func _ready():
    var errno = GlobalMatchState.connect("selected_unit_changed", self, "_on_selected_unit_changed")
    assert(errno == OK)
    _on_selected_unit_changed()


func _on_selected_unit_changed():
    var unit = GlobalMatchState.get_selected_unit()
    if unit:
        text = GlobalMatchState.get_selected_unit().get_display_name()
    else:
        text = ""
