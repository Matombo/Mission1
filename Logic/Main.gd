extends Node


func _unhandled_input(event):
    if GlobalMatchState.initialized and event.is_action_pressed("ui_cancel"):
        toggle_menu()


func toggle_menu():
    var menu = get_node("Menu")
    if menu.visible:
        menu.hide()
    else:
        menu.show()
