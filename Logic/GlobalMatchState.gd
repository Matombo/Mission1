extends Node

# Signals
signal active_player_changed(last_player)
signal selected_unit_changed
signal match_initialized
signal match_destroyed

# State
var hud_handle = null
var map_container_handle = null
var initialized = false
var loaded_map = null
var players = []
var active_player_id = 0
var selected_unit = null


func get_initialized():
    return initialized


func get_loaded_map():
    return loaded_map


func get_friendly_units():
    # TODO implement teams
    return players[active_player_id].get_units()


func get_active_player():
    if (players):
        return players[active_player_id]
    else:
        return null


func get_selected_unit():
    return selected_unit


func init(new_hud_handle, new_map_container_handle, new_map, new_players):
    if initialized:
        destroy()
    
    hud_handle = new_hud_handle
    map_container_handle = new_map_container_handle
    loaded_map = load("res://Maps/" + new_map + ".tscn").instance()
    map_container_handle.add_child(loaded_map)
    players = new_players
    
    active_player_id = 0
    emit_signal("active_player_changed", players[0])
    selected_unit = null
    initialized = true
    emit_signal("match_initialized")


func destroy():
    hud_handle = null
    map_container_handle = null
    loaded_map.queue_free()
    loaded_map = null
    players = []
    active_player_id = 0
    selected_unit = null
    initialized = false
    emit_signal("match_destroyed")


func select_unit(unit = null):
    if selected_unit:
        selected_unit.deselect()
    if unit:
        unit.select()
    selected_unit = unit
    emit_signal("selected_unit_changed")
    

func next_player():
    select_unit()
    var last_player_id = active_player_id
    active_player_id += 1
    if active_player_id >= players.size():
        active_player_id = 0
    emit_signal("active_player_changed", players[last_player_id])
